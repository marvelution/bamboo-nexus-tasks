/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.rest;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.sonatype.nexus.client.core.Condition;
import org.sonatype.nexus.client.core.NexusErrorMessageException;
import org.sonatype.nexus.client.core.spi.SubsystemFactory;
import org.sonatype.nexus.client.rest.ConnectionInfo;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClientFactory;

import com.atlassian.bamboo.rest.entity.RestResponse;
import com.atlassian.sal.api.message.I18nResolver;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServerManager;

/**
 * Testcase for the {@link StagingResource} REST resource
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class StagingResourceTest {

	private StagingResource resource;

	@Mock
	private NexusServerManager serverManager;
	@Mock
	private I18nResolver i18nResolver;
	private JerseyNexusClientFactory nexusClientFactory;
	@Mock
	private JerseyNexusClient nexusClient;

	@Before
	public void before() {
		nexusClientFactory = new JerseyNexusClientFactory() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected JerseyNexusClient doCreateFor(Condition connectionCondition,
						SubsystemFactory<?, JerseyNexusClient>[] subsystemFactories, ConnectionInfo connectionInfo) {
				return nexusClient;
			}
		};
		when(i18nResolver.getText(anyString())).thenAnswer(new Answer<String>() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}
		});
		resource = new StagingResource(serverManager, i18nResolver);
		resource.setJerseyNexusClientFactory(nexusClientFactory);
	}

	@Test
	public void testGetStagingProfilesWithInvalidNexusServer() throws Exception {
		when(serverManager.getServer(1)).thenReturn(null);
		Response response = resource.getStagingProfiles(null, null, null, false, 1);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(response.getEntity(), instanceOf(RestResponse.class));
		verify(i18nResolver, times(1)).getText("nexus.rest.invalid.server.id");
		verify(serverManager, times(1)).getServer(1);
	}

	@Test
	public void testGetStagingProfilesNoBaseUrl() throws Exception {
		Response response = resource.getStagingProfiles(null, null, null, false, 0);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(response.getEntity(), instanceOf(RestResponse.class));
		verify(i18nResolver, times(1)).getText("nexus.rest.no.base.url");
	}

	@Test
	public void testGetStagingProfilesThrowingMalformedURLException() throws Exception {
		Response response = resource.getStagingProfiles("I'm not and Url", null, null, false, 0);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(response.getEntity(), instanceOf(RestResponse.class));
		verify(i18nResolver, times(1)).getText("nexus.rest.malformed.base.url");
	}

	@Test
	public void testGetStagingProfilesThrowingOtherException() throws Exception {
		when(nexusClient.serviceResource("/staging/profiles")).thenThrow(
			new NexusErrorMessageException(401, "I Failed", null));
		Response response =
			resource.getStagingProfiles("http://repository.marvelution.com", "username", "password", false, 0);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(response.getEntity(), instanceOf(RestResponse.class));
		verify(i18nResolver, times(1)).getText("nexus.rest.unexpected.error");
	}

}
