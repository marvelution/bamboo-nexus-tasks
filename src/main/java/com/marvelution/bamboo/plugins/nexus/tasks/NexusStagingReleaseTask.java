/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks;

import org.apache.commons.lang.StringUtils;
import org.sonatype.nexus.client.core.NexusErrorMessageException;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.sonatype.nexus.staging.client.StagingWorkflowV2Service;

/**
 * {@link AbstractNexusStagingTask} implementation to perform the staging release action
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class NexusStagingReleaseTask extends AbstractNexusStagingTask {

	/**
	 * Constructor
	 * 
	 * @param capabilityContext the {@link CapabilityContext} implementation
	 * @param environmentVariableAccessor the {@link EnvironmentVariableAccessor} implementation
	 */
	protected NexusStagingReleaseTask(CapabilityContext capabilityContext,
									EnvironmentVariableAccessor environmentVariableAccessor) {
		super(capabilityContext, environmentVariableAccessor);
	}

	@Override
	protected void doExecute(String[] stagingRepositoryIds, StagingWorkflowV2Service stagingService,
					TaskContext taskContext) throws NexusErrorMessageException {
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		buildLogger.addBuildLogEntry("Releasing staging repository with IDs: "
			+ StringUtils.join(stagingRepositoryIds, ", "));
		stagingService.releaseStagingRepositories(taskContext.getConfigurationMap().get(CFG_DESCRIPTION),
			stagingRepositoryIds);
	}

}
