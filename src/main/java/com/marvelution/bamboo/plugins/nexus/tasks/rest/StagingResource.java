/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.rest;

import java.net.MalformedURLException;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClient;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClientFactory;

import com.atlassian.bamboo.rest.entity.RestResponse;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableSet;
import com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServer;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServerManager;
import com.marvelution.bamboo.plugins.nexus.tasks.utils.NexusClientUtils;
import com.sonatype.nexus.staging.api.dto.StagingProfileDTO;
import com.sonatype.nexus.staging.api.dto.StagingProfileListResponseDTO;
import com.sonatype.nexus.staging.client.internal.ExceptionConverter;
import com.sonatype.nexus.staging.client.internal.JerseyStagingWorkflow;
import com.sun.jersey.spi.resource.Singleton;

/**
 * Staging related REST resources
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@Path("/staging")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
public class StagingResource {

	private static final Logger LOGGER = Logger.getLogger(StagingResource.class);
	private static final String DEPLOY_MODE = "DEPLOY";
	private static final String UPLOAD_MODE = "UPLOAD";
	private static final String UPLOAD_AND_DEPLOY_MODE = "BOTH";
	private static final String GROUP_MODE = "GROUP";

	private final NexusServerManager serverManager;
	private final I18nResolver i18nResolver;
	@SuppressWarnings("unchecked")
	private JerseyNexusClientFactory clientFactory = new JerseyNexusClientFactory();

	/**
	 * Constructor
	 *
	 * @param serverManager the {@link NexusServerManager}
	 * @param i18nResolver the {@link I18nResolver}
	 */
	public StagingResource(NexusServerManager serverManager, I18nResolver i18nResolver) {
		this.serverManager = serverManager;
		this.i18nResolver = i18nResolver;
	}

	/**
	 * {@link POST} endpoint to get all the staging profiles
	 * 
	 * @param baseUrl the Nexus base URL
	 * @param username the username of the user to authenticate as
	 * @param password the password of the given user
	 * @param encrypted flag if the given password is encrypted
	 * @param nexusId the ID of a {@link NexusServer}
	 * @return {@link Set} of all the Nexus Staging Profiles
	 */
	@POST
	@Path("/profiles/staging")
	public Response getStagingProfiles(@FormParam("baseUrl") String baseUrl, @FormParam("username") String username,
					@FormParam("password") String password, @FormParam("encrypted") boolean encrypted,
					@FormParam("nexusId") int nexusId) {
		return getProfiles(baseUrl, username, password, encrypted, nexusId,
			ImmutableSet.of(DEPLOY_MODE, UPLOAD_MODE, UPLOAD_AND_DEPLOY_MODE));
	}

	/**
	 * {@link POST} endpoint to get all the staging build promotion profiles
	 * 
	 * @param baseUrl the Nexus base URL
	 * @param username the username of the user to authenticate as
	 * @param password the password of the given user
	 * @param encrypted flag if the given password is encrypted
	 * @param nexusId the ID of a {@link NexusServer}
	 * @return {@link Set} of all the Nexus Staging Build Promotion Profiles
	 */
	@POST
	@Path("/profiles/buildPromotion")
	public Response getBuildPromotionProfiles(@FormParam("baseUrl") String baseUrl,
					@FormParam("username") String username, @FormParam("password") String password,
					@FormParam("encrypted") boolean encrypted, @FormParam("nexusId") int nexusId) {
		return getProfiles(baseUrl, username, password, encrypted, nexusId, ImmutableSet.of(GROUP_MODE));
	}

	/**
	 * Getter of all the Nexus Staging profiles
	 * 
	 * @param baseUrl the Nexus base URL
	 * @param username the username of the user to authenticate as
	 * @param password the password of the given user
	 * @param encrypted flag if the given password is encrypted
	 * @param nexusId the ID of a {@link NexusServer}
	 * @param modes Mode filter for the profiles
	 * @return the {@link Set} of all Nexus Profiles that match the given modes
	 */
	@SuppressWarnings("rawtypes")
	private Response getProfiles(String baseUrl, String username, String password, boolean encrypted, int nexusId,
					Set<String> modes) {
		RestResponse.Builder builder = RestResponse.builder();
		if (nexusId > 0) {
			final NexusServer server = serverManager.getServer(nexusId);
			if (server == null) {
				return Response.ok(
					builder.error(i18nResolver.getText("nexus.rest.invalid.server.id")).build(RestResponse.class))
					.build();
			}
			baseUrl = server.getHost();
			username = server.getUsername();
			password = server.getPassword();
		} else if (encrypted && StringUtils.isNotBlank(password)) {
			password = NexusConfigurationConstants.ENCRYPTOR.decrypt(password);
		}
		LOGGER.info("Getting profiles from " + baseUrl + " that match type(s): " + StringUtils.join(modes, ", "));
		if (StringUtils.isBlank(baseUrl)) {
			return Response.ok(
				builder.error(i18nResolver.getText("nexus.rest.no.base.url")).build(RestResponse.class)).build();
		}
		try {
			final JerseyNexusClient client =
				(JerseyNexusClient) clientFactory.createFor(NexusClientUtils.getConnectionInfo(baseUrl, username,
					password));
			StagingProfileListResponseDTO clientResponse =
				(StagingProfileListResponseDTO) new ExceptionConverter(new JerseyStagingWorkflow(client)) {

					@Override
					public StagingProfileListResponseDTO perform() {
						return ((StagingProfileListResponseDTO) client.serviceResource(
							"staging/profiles").get(StagingProfileListResponseDTO.class));
					}
				}.runAndReturn();
			// Filter the StagingProfiles before returning them
			StagingProfileListResponseDTO response = new StagingProfileListResponseDTO();
			for (StagingProfileDTO profile : clientResponse.getData()) {
				if (modes.contains(profile.getMode())) {
					response.addStagingProfile(profile);
				}
			}
			return Response.ok(response).build();
		} catch (MalformedURLException e) {
			return Response.ok(
				builder.error(i18nResolver.getText("nexus.rest.malformed.base.url")).build(RestResponse.class))
				.build();
		} catch (Exception e) {
			LOGGER.debug(e);
			return Response.ok(
				builder.error(i18nResolver.getText("nexus.rest.unexpected.error")).error(e.getMessage())
					.build(RestResponse.class)).build();
		}
	}

	/**
	 * Setter for clientFactory
	 *
	 * @param clientFactory the clientFactory to set
	 */
	/* package */ void setJerseyNexusClientFactory(JerseyNexusClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

}
