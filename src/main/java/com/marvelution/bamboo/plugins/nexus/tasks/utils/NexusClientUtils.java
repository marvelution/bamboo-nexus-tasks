/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.utils;

import java.net.MalformedURLException;

import org.apache.commons.lang.StringUtils;
import org.sonatype.nexus.client.rest.AuthenticationInfo;
import org.sonatype.nexus.client.rest.BaseUrl;
import org.sonatype.nexus.client.rest.ConnectionInfo;
import org.sonatype.nexus.client.rest.UsernamePasswordAuthenticationInfo;


/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class NexusClientUtils {

	public static ConnectionInfo getConnectionInfo(String baseUrl, String username, String password) throws MalformedURLException {
		AuthenticationInfo authenticationInfo = null;
		if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
			authenticationInfo = new UsernamePasswordAuthenticationInfo(username, password);
		}
		return new ConnectionInfo(BaseUrl.baseUrlFrom(baseUrl), authenticationInfo, null);
	}

}
