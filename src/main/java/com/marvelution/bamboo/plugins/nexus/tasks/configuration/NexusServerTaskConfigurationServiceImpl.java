/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.configuration;

import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CTX_ADMIN_ACTION;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_NEXUS_ID;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_NEXUS_URL;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_NEXUS_USERNAME;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_NEXUS_PASSWORD;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.task.ImmutableTaskDefinition;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.bamboo.plugins.nexus.tasks.predicates.NexusTaskPredicates;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServer;
import com.marvelution.bamboo.plugins.nexus.tasks.utils.NexusTaskUtils;

/**
 * Default implementation of the {@link NexusServerTaskConfigurationService} interface
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class NexusServerTaskConfigurationServiceImpl implements NexusServerTaskConfigurationService {

	private final Logger logger = Logger.getLogger(NexusServerTaskConfigurationServiceImpl.class);
	private final PlanManager planManager;
	private final EventPublisher eventPublisher;
	private TaskManager taskManager;
	private TaskConfigurationService taskConfigurationService;

	/**
	 * Constructor
	 *
	 * @param planManager    the {@link PlanManager} implementation
	 * @param eventPublisher the {@link EventPublisher} implementation
	 */
	public NexusServerTaskConfigurationServiceImpl(PlanManager planManager, EventPublisher eventPublisher) {
		this.planManager = planManager;
		this.eventPublisher = eventPublisher;
	}

	@Override
	public boolean updateNexusServerTaskConfiguration(NexusServer server) {
		Map<PlanKey, List<TaskDefinition>> validTaskDefinitions = Maps.newHashMap();
		for (Job job : getNexusServerDependingJobs(server)) {
			logger.debug("Checking Job [" + job.getKey() + "] for Nexus Server dependent tasks");
			for (TaskDefinition taskDefinition : Iterables.filter(job.getBuildDefinition().getTaskDefinitions(),
					NexusTaskPredicates.isNexusServerDependingTask(server))) {
				logger.debug("Checking TaskDefinition " + taskDefinition.getId() + " of type ["
						+ taskDefinition.getPluginKey() + "]");
				TaskDefinition currectTaskDefinition = new ImmutableTaskDefinition(taskDefinition);
				if (isUpdatedTaskConfigurationValid(currectTaskDefinition, server)) {
					logger.debug("Updated Server is valid for this Nexus Task");
					PlanKey key = PlanKeys.getPlanKey(job.getKey());
					if (validTaskDefinitions.containsKey(key)) {
						validTaskDefinitions.get(key).add(currectTaskDefinition);
					} else {
						validTaskDefinitions.put(key, Lists.newArrayList(currectTaskDefinition));
					}
				} else {
					logger.error("Failed to validate the updated Nexus Server [" + server.getID() + "] for task "
							+ taskDefinition.getId() + ". Cancelling the update of the task definitions.");
					return false;
				}
			}
		}
		// Still here? Then all the task definitions checked are valid and the update can start.
		logger.debug("All tasks are valid with the new Server configuration. Start the update of all the tasks.");
		for (Map.Entry<PlanKey, List<TaskDefinition>> entry : validTaskDefinitions.entrySet()) {
			for (TaskDefinition taskDefinition : entry.getValue()) {
				TaskDefinition newTaskDefinition = getTaskConfigurationService().editTask(entry.getKey(),
						taskDefinition.getId(), taskDefinition.getUserDescription(),
						getTaskConfigurationMap(taskDefinition, server), taskDefinition.getRootDirectorySelector());
				logger.info("Updated task [" + newTaskDefinition.getId() + "] of type ["
						+ newTaskDefinition.getPluginKey() + "]");
			}
			publishBuildConfigurationUpdatedEventForPlan(entry.getKey());
		}
		return true;
	}

	@Override
	public void disableNexusServerTaskJobs(NexusServer server) {
		for (Job job : getNexusServerDependingJobs(server)) {
			logger.info("Suspending all build activity for job " + job.getKey());
			job.setSuspendedFromBuilding(true);
			planManager.setPlanSuspendedState(job.getPlanKey(), true);
			publishBuildConfigurationUpdatedEventForPlan(job.getPlanKey());
		}
	}

	/**
	 * Getter for taskManager
	 *
	 * @return the taskManager
	 */
	public TaskManager getTaskManager() {
		logger.debug("Getting the TaskManager implementation");
		if (taskManager == null) {
			logger.debug("Getting the TaskManager from the Spring Container Manager");
			taskManager = (TaskManager) ContainerManager.getComponent("taskManager");
		}
		return taskManager;
	}

	/**
	 * Getter for taskConfigurationService
	 *
	 * @return the taskConfigurationService
	 */
	public TaskConfigurationService getTaskConfigurationService() {
		logger.debug("Getting the TaskConfigurationService implementation");
		if (taskConfigurationService == null) {
			logger.debug("Getting the TaskConfigurationService from the Spring Container Manager");
			taskConfigurationService =
					(TaskConfigurationService) ContainerManager.getComponent("taskConfigurationService");
		}
		return taskConfigurationService;
	}

	/**
	 * Internal method to publish the {@link BuildConfigurationUpdatedEvent} for the given {@link PlanKey}
	 *
	 * @param planKey the {@link PlanKey} to publish the {@link BuildConfigurationUpdatedEvent} for
	 */
	private void publishBuildConfigurationUpdatedEventForPlan(PlanKey planKey) {
		logger.debug("Publishing the BuildConfigrationUpdatedEvent for plan " + planKey.getKey());
		eventPublisher.publish(new BuildConfigurationUpdatedEvent(this, planKey));
	}

	/**
	 * Internal method get all the {@link Job} instances that have a Nexus Task that depend on the given
	 * {@link NexusServer}
	 *
	 * @param server the {@link NexusServer} to check for
	 * @return the Collection of {@link Job} instances that depend on the given {@link NexusServer}
	 */
	private Collection<Job> getNexusServerDependingJobs(NexusServer server) {
		List<Job> jobs = Lists.newArrayList();
		for (Plan plan : planManager.getAllPlans()) {
			jobs.addAll(NexusTaskUtils.getJobsWithNexusTasks(plan,
					NexusTaskPredicates.isNexusServerDependingTask(server)));
		}
		return jobs;
	}

	/**
	 * Internal method to validate the new configuration using the task configurator
	 *
	 * @param currectTaskDefinition the current {@link TaskDefinition}
	 * @param server                the new {@link NexusServer}
	 * @return <code>true</code> if the new configuration validates, <code>false</code> otherwise
	 */
	private boolean isUpdatedTaskConfigurationValid(TaskDefinition currectTaskDefinition, NexusServer server) {
		TaskConfigurator configurator = getTaskConfigurator(currectTaskDefinition);
		if (configurator != null) {
			SimpleErrorCollection errorCollection = new SimpleErrorCollection();
			configurator.validate(getActionParametersMap(currectTaskDefinition, server), errorCollection);
			return !errorCollection.hasAnyErrors();
		}
		return true;
	}

	/**
	 * Get the Task Configuration {@link Map} with the updated {@link NexusServer} configuration
	 *
	 * @param taskDefinition the current {@link TaskDefinition}
	 * @param server         the new {@link NexusServer}
	 * @return the {@link Map<String, String>} task configuration map
	 */
	private Map<String, String> getTaskConfigurationMap(TaskDefinition taskDefinition, NexusServer server) {
		TaskConfigurator configurator = getTaskConfigurator(taskDefinition);
		if (configurator != null) {
			return configurator.generateTaskConfigMap(getActionParametersMap(taskDefinition, server), taskDefinition);
		} else {
			return getConfigurationMap(taskDefinition.getConfiguration(), server);
		}
	}

	/**
	 * Internal method to the configuration parameters as an {@link ActionParametersMap} implementation
	 *
	 * @param taskDefinition the {@link TaskDefinition}
	 * @param server         the {@link NexusServer}
	 * @return the {@link ActionParametersMap}
	 */
	private ActionParametersMap getActionParametersMap(TaskDefinition taskDefinition, NexusServer server) {
		return new ActionParametersMapImpl(getConfigurationMap(taskDefinition.getConfiguration(), server));
	}

	/**
	 * Get the {@link Map<String, String>} for the Task Configuration including the new Server settings
	 *
	 * @param currentConfiguration the current configuration {@link Map}
	 * @param server               the new {@link NexusServer}
	 * @return the {@link Map<String, String>}
	 */
	private Map<String, String> getConfigurationMap(Map<String, String> currentConfiguration,
	                                                NexusServer server) {
		Map<String, String> config = Maps.newHashMap(currentConfiguration);
		config.put(CFG_NEXUS_ID, String.valueOf(server.getID()));
		config.put(CFG_NEXUS_URL, server.getHost());
		config.put(CFG_NEXUS_USERNAME, server.getUsername());
		config.put(CFG_NEXUS_PASSWORD, server.getPassword());
		config.put(CTX_ADMIN_ACTION, "true");
		return config;
	}

	/**
	 * Get the {@link TaskConfigurator} for the {@link TaskDefinition} given
	 *
	 * @param taskDefinition the {@link TaskDefinition} to get eh {@link TaskConfigurator} for
	 * @return the {@link TaskConfigurator} if the task has one, {@code null} is returned otherwise
	 */
	private TaskConfigurator getTaskConfigurator(TaskDefinition taskDefinition) {
		TaskModuleDescriptor descriptor = getTaskManager().getTaskDescriptor(taskDefinition.getPluginKey());
		if (descriptor != null && descriptor.getTaskConfigurator() != null) {
			return descriptor.getTaskConfigurator();
		}
		return null;
	}

}
