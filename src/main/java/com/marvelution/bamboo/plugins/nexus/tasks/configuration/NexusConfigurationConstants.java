/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.configuration;

import com.marvelution.security.crypto.SimpleStringEncryptor;
import com.marvelution.security.crypto.StringEncryptor;

/**
 * Constants list for the plugin
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public interface NexusConfigurationConstants {

	/**
	 * Configuration variable names
	 */
	String CFG_BUILDER_LABEL = "label";
	String CFG_NEXUS_ID = "nexusId";
	String CFG_NEXUS_URL = "hostUrl";
	String CFG_NEXUS_USERNAME = "hostUsername";
	String CFG_NEXUS_PASSWORD = "hostPassword";
	String CFG_STAGING_PROFILE_ID = "stagingProfileId";
	String CFG_BUILD_PROMOTION_PROFILE_ID = "buildPromotionProfileId";
	String CFG_GAV_OPTION = "gavOption";
	String CFG_AUTO_DISCOVER_GAV = "autoDiscoverGav";
	String CFG_PROVIDE_GAV = "provideGav";
	String CFG_POM_FILE = "mavenPom";
	String CFG_GROUP_ID = "groupId";
	String CFG_ARTIFACT_ID = "artifactId";
	String CFG_VERSION = "version";
	String CFG_DESCRIPTION = "description";
	String CFG_DROP_ON_CLOSE_FAILURE = "dropOnCloseFailure";

	/**
	 * Configuration screen Context variable names
	 */
	String CTX_UI_CONFIG_BEAN = "uiConfigBean";
	String CTX_GAV_OPTIONS = "gavOptions";
	String CTX_USES_AUTO_GAV = "usesAutoGav";
	String CTX_NEXUS_SERVERS = "nexusServers";
	String CTX_NEXUS_SERVER = "nexusServer";
	String CTX_DELETED_SERVER = "deletedServer";
	String CTX_ADMIN_ACTION = "adminAction";
	String CTX_FAKE_PASSWORD = "******";
	String CTX_CHANGE_PASSWORD = "host.password.change";
	String CTX_TEMPORARY_PASSWORD = "host.temporary.password";
	String CTX_IMPLICIT_PROFILE_ID = "implicit";

	/**
	 * Defaults
	 */
	String DEFAULT_MAVEN_POM = "pom.xml";

	/**
	 * {@link StringEncryptor} used for encrypting/decrypting configuration settings
	 */
	StringEncryptor ENCRYPTOR = new SimpleStringEncryptor("CREqezaHUBer658ph52EDR8Z2Cates");

}
