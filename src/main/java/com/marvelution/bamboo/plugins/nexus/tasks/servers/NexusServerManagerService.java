/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.servers;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import org.apache.log4j.Logger;

import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.collect.Lists;

/**
 * Default {@link NexusServerManager} implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class NexusServerManagerService implements NexusServerManager {

	private final Logger logger = Logger.getLogger(NexusServerManagerService.class);
	private final ActiveObjects objects;

	/**
	 * Constructor
	 *
	 * @param objects the {@link ActiveObjects} implementation
	 */
	public NexusServerManagerService(ActiveObjects objects) {
		this.objects = checkNotNull(objects, "active objects");
	}

	@Override
	public boolean isConfigured() {
		return hasServers();
	}

	@Override
	public boolean hasServers() {
		return objects.executeInTransaction(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction() {
				return objects.count(NexusServer.class) > 0;
			}
		});
	}

	@Override
	public boolean hasServer(int serverId) {
		return getServer(serverId) != null;
	}

	@Override
	public boolean hasServer(String name) {
		return getServer(name) != null;
	}

	@Override
	public Collection<NexusServer> getServers() {
		return objects.executeInTransaction(new TransactionCallback<Collection<NexusServer>>() {
			@Override
			public Collection<NexusServer> doInTransaction() {
				return Lists.newArrayList(objects.find(NexusServer.class));
			}
		});
	}

	@Override
	public NexusServer getServer(final int serverId) {
		return objects.executeInTransaction(new TransactionCallback<NexusServer>() {
			@Override
			public NexusServer doInTransaction() {
				return objects.get(NexusServer.class, serverId);
			}
		});
	}

	@Override
	public NexusServer getServer(final String name) {
		return objects.executeInTransaction(new TransactionCallback<NexusServer>() {
			@Override
			public NexusServer doInTransaction() {
				try {
					return objects.find(NexusServer.class, Query.select().where("NAME = ?", name).limit(1))[0];
				} catch (IndexOutOfBoundsException e) {
					return null;
				}
			}
		});
	}

	@Override
	public NexusServer addServer(final String name, final String description, final String host,
					final String username, final String password) {
		return objects.executeInTransaction(new TransactionCallback<NexusServer>() {
			@Override
			public NexusServer doInTransaction() {
				logger.debug("Adding a new server with name " + name);
				NexusServer server = objects.create(NexusServer.class, new DBParam("NAME", name),
					new DBParam("HOST", host), new DBParam("USERNAME", username), new DBParam("PASSWORD", password));
				server.setName(name);
				server.setDescription(description);
				server.setHost(host);
				server.setUsername(username);
				server.setPassword(password);
				server.save();
				return server;
			}
		});
	}

	@Override
	public NexusServer updateServer(final int serverId, final String name, final String description,
					final String host, final String username, final String password) {
		return objects.executeInTransaction(new TransactionCallback<NexusServer>() {
			@Override
			public NexusServer doInTransaction() {
				NexusServer server = getServer(serverId);
				logger.debug("Updating server with ID " + serverId);
				checkNotNull(server, "server not found");
				server.setName(name);
				server.setDescription(description);
				server.setHost(host);
				server.setUsername(username);
				server.setPassword(password);
				server.save();
				return server;
			}
		});
	}

	@Override
	public void removeServer(final int serverId) {
		objects.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				NexusServer server = objects.get(NexusServer.class, serverId);
				checkNotNull(server, "server argument may NOT be null");
				logger.debug("Deleting server with ID: " + server.getID());
				objects.delete(server);
				return null;
			}
		});
	}

}
