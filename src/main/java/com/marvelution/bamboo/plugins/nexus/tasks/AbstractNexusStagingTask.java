/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.sonatype.nexus.client.core.NexusClient;
import org.sonatype.nexus.client.core.NexusErrorMessageException;
import org.sonatype.nexus.client.rest.ConnectionInfo;
import org.sonatype.nexus.client.rest.jersey.JerseyNexusClientFactory;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants;
import com.marvelution.bamboo.plugins.nexus.tasks.gav.GAVInfo;
import com.marvelution.bamboo.plugins.nexus.tasks.utils.NexusClientUtils;
import com.sonatype.nexus.staging.client.Profile;
import com.sonatype.nexus.staging.client.ProfileMatchingParameters;
import com.sonatype.nexus.staging.client.StagingRepository;
import com.sonatype.nexus.staging.client.StagingRuleFailures;
import com.sonatype.nexus.staging.client.StagingRuleFailuresException;
import com.sonatype.nexus.staging.client.StagingWorkflowV2Service;
import com.sonatype.nexus.staging.client.StagingRuleFailures.RuleFailure;
import com.sonatype.nexus.staging.client.rest.JerseyStagingWorkflowV2SubsystemFactory;

/**
 * Base {@link TaskType} implementation for Nexus Staging actions
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public abstract class AbstractNexusStagingTask implements TaskType, NexusConfigurationConstants {

	protected final CapabilityContext capabilityContext;
	protected final EnvironmentVariableAccessor environmentVariableAccessor;

	/**
	 * Constructor
	 *
	 * @param capabilityContext           the {@link CapabilityContext} implementation
	 * @param environmentVariableAccessor the {@link EnvironmentVariableAccessor} implementation
	 */
	protected AbstractNexusStagingTask(CapabilityContext capabilityContext,
	                                   EnvironmentVariableAccessor environmentVariableAccessor) {
		this.capabilityContext = capabilityContext;
		this.environmentVariableAccessor = environmentVariableAccessor;
	}

	@NotNull
	@Override
	public final TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);
		final Map<String, String> configuration = taskContext.getConfigurationMap();
		try {
			ConnectionInfo connectionInfo = NexusClientUtils.getConnectionInfo(configuration.get(CFG_NEXUS_URL),
					configuration.get(CFG_NEXUS_USERNAME), ENCRYPTOR.decrypt(configuration.get(CFG_NEXUS_PASSWORD)));
			final NexusClient client =
					new JerseyNexusClientFactory(new JerseyStagingWorkflowV2SubsystemFactory()).createFor(connectionInfo);
			final StagingWorkflowV2Service stagingService = client.getSubsystem(StagingWorkflowV2Service.class);
			String[] stageRepositoryIds = null;
			if (StringUtils.isNotBlank(configuration.get(CFG_STAGING_PROFILE_ID))
					&& !CTX_IMPLICIT_PROFILE_ID.equals(configuration.get(CFG_STAGING_PROFILE_ID))) {
				stageRepositoryIds = new String[] { configuration.get(CFG_STAGING_PROFILE_ID) };
			} else {
				GAVInfo gavInfo = GAVInfo.get(taskContext);
				Profile profile = stagingService.matchProfile(new ProfileMatchingParameters(gavInfo.getGroupId(),
						gavInfo.getGroupId(), gavInfo.getVersion()));
				if (profile != null) {
					List<StagingRepository> repos = stagingService.listStagingRepositories(profile.getId());
					if (repos != null && repos.size() > 0) {
						stageRepositoryIds = Lists.transform(repos, new Function<StagingRepository, String>() {
							@Override
							public String apply(StagingRepository from) {
								return from.getId();
							}
						}).toArray(new String[repos.size()]);
					}
				}
			}
			if (stageRepositoryIds == null || stageRepositoryIds.length == 0) {
				buildLogger.addErrorLogEntry("Unable to determine the staging repository to operate against!");
				taskResultBuilder.failedWithError();
			} else {
				try {
					doExecute(stageRepositoryIds, stagingService, taskContext);
					taskResultBuilder.success();
				} catch (NexusErrorMessageException e) {
					buildLogger.addErrorLogEntry("Could not perform action: Nexus ErrorResponse received", e);
					taskResultBuilder.failedWithError();
				} catch (StagingRuleFailuresException e) {
					buildLogger.addErrorLogEntry("Failed staging rules prevented the action execution", e);
					logStagingRuleFailureException(e, buildLogger);
					taskResultBuilder.failedWithError();
				}
			}
		} catch (MalformedURLException e) {
			buildLogger.addErrorLogEntry("Malformed Nexus base URL!", e);
			taskResultBuilder.failedWithError();
		}
		return taskResultBuilder.build();
	}

	/**
	 * Execute the stage action for the given staging repository using the given {@link NexusClient}
	 *
	 * @param stagingRepositoryIds the array of staging repository IDs to action on
	 * @param stagingService       the {@link StagingWorkflowV2Service}
	 * @param taskContext          the {@link TaskContext}
	 * @throws NexusErrorMessageException in case of an Nexus Error Response
	 */
	protected abstract void doExecute(final String[] stagingRepositoryIds,
	                                  final StagingWorkflowV2Service stagingService,
	                                  final TaskContext taskContext)  throws NexusErrorMessageException;

	/**
	 * Log the rule failures in the {@link StagingRuleFailuresException} to the given {@link BuildLogger}
	 *
	 * @param exception the {@link StagingRuleFailuresException}
	 * @param logger    the {@link BuildLogger}
	 */
	protected void logStagingRuleFailureException(StagingRuleFailuresException exception, BuildLogger logger) {
		logger.addErrorLogEntry("Staging rule failures prevented the closure of staging repositories");
		for (StagingRuleFailures rule : exception.getFailures()) {
			logger.addErrorLogEntry(String.format("Repository \"%s\" (id=%s) failure", rule.getRepositoryName(),
					rule.getRepositoryId()));
			for (RuleFailure ruleFailure : rule.getFailures()) {
				logger.addErrorLogEntry(String.format("  Rule \"%s\" failures", ruleFailure.getRuleName()));
				for (String message : ruleFailure.getMessages()) {
					logger.addErrorLogEntry(String.format("    * %s", message));
				}
			}
		}
	}

}
