/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.gav;

import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_AUTO_DISCOVER_GAV;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_ARTIFACT_ID;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_GAV_OPTION;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_GROUP_ID;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_POM_FILE;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_VERSION;
import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.DEFAULT_MAVEN_POM;

import java.io.File;
import java.io.FileReader;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.TaskContext;

/**
 * GroupId, ArtifactId and Version (G.A.V.) info
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public abstract class GAVInfo {

	protected String groupId = null;
	protected String artifactId = null;
	protected String version = null;

	/**
	 * Get the groupId of the released artifact
	 *
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Get the artifactId of the released artifact
	 *
	 * @return the artifactId
	 */
	public String getArtifactId() {
		return artifactId;
	}

	/**
	 * Get the version of the released artifact
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Check if the GAV is valid
	 *
	 * @return <code>true</code> if valid, <code>false</code> otherwise
	 */
	public boolean isValid() {
		return StringUtils.isNotBlank(groupId) && StringUtils.isNotBlank(artifactId);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getGroupId()).append(":").append(getArtifactId());
		if (StringUtils.isNotBlank(getVersion())) {
			builder.append(":").append(getVersion());
		}
		return builder.toString();
	}

	/**
	 * Helper method to get the correct {@link GAVInfo}
	 *
	 * @param taskContext the {@link TaskContext}
	 */
	public static GAVInfo get(TaskContext taskContext) {
		if (CFG_AUTO_DISCOVER_GAV.equals(taskContext.getConfigurationMap().get(CFG_GAV_OPTION))) {
			return new AutomaticDisoveryGAVInfo(taskContext.getWorkingDirectory(), taskContext.getConfigurationMap().get(CFG_POM_FILE));
		} else {
			return new PreConfiguredGAVInfo(taskContext.getConfigurationMap());
		}
	}

	/**
	 * {@link GAVInfo} implementation to get the GAV from the Maven POM file
	 *
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 * @since 1.0.0
	 */
	public static class AutomaticDisoveryGAVInfo extends GAVInfo {

		/**
		 * Constructor
		 *
		 * @param workingDirectory the task working directory
		 * @param pomFile          the POM file name, defaults to {@link com.marvelution.bamboo.plugins.nexus.tasks
		 *                         .configuration.NexusConfigurationConstants#DEFAULT_MAVEN_POM} if {@code null or {@code
		 *                         empty}}
		 */
		AutomaticDisoveryGAVInfo(File workingDirectory, String pomFile) {
			if (StringUtils.isBlank(pomFile)) {
				pomFile = DEFAULT_MAVEN_POM;
			}
			File pom = new File(workingDirectory, pomFile);
			try {
				Model model = new MavenXpp3Reader().read(new FileReader(pom));
				if (StringUtils.isBlank(model.getGroupId())) {
					groupId = model.getParent().getGroupId();
				} else {
					groupId = model.getGroupId();
				}
				artifactId = model.getArtifactId();
				version = model.getVersion();
			} catch (Exception e) {
				// We can just ignore any exceptions here
				// Having no groupId or artifactId will raise a failure in the task
			}
		}
	}

	/**
	 * {@link GAVInfo} implementation to get the GAV from the task configuration map
	 *
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 * @since 1.0.0
	 */
	public static class PreConfiguredGAVInfo extends GAVInfo {

		/**
		 * Constructor
		 *
		 * @param configuration the {@link ConfigurationMap}
		 */
		public PreConfiguredGAVInfo(ConfigurationMap configuration) {
			groupId = configuration.get(CFG_GROUP_ID);
			artifactId = configuration.get(CFG_ARTIFACT_ID);
			version = configuration.get(CFG_VERSION);
		}

	}

}
