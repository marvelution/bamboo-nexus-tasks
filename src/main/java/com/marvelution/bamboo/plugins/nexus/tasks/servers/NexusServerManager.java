/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.servers;

import java.util.Collection;

/**
 * {@link NexusServer} manager interface
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public interface NexusServerManager {

	/**
	 * Get if at least one {@link NexusServer} is configured
	 * 
	 * @return <code>true</code> if at least one {@link NexusServer} is configured, <code>false</code> otherwise
	 * @see #hasServers()
	 */
	boolean isConfigured();

	/**
	 * Check if there are any {@link NexusServer} objects available
	 * 
	 * @return <code>true</code> if there are {@link NexusServer} available, <code>false</code> otherwise
	 */
	boolean hasServers();

	/**
	 * Check if there is a {@link NexusServer} with the given Id
	 * 
	 * @param serverId the {@link NexusServer} Id to check
	 * @return <code>true</code> if the server with the given Id exists, <code>false</code> otherwise
	 */
	boolean hasServer(int serverId);

	/**
	 * Check if there is a {@link NexusServer} with the given Name
	 * 
	 * @param name the {@link NexusServer} Name to check
	 * @return <code>true</code> if the server with the given Name exists, <code>false</code> otherwise
	 */
	boolean hasServer(String name);

	/**
	 * Getter for the {@link Collection} of all configured {@link NexusServer} objects
	 * 
	 * @return the {@link Collection} of {@link NexusServer} objects
	 */
	Collection<NexusServer> getServers();

	/**
	 * Getter for a {@link NexusServer} by its Id
	 * 
	 * @param serverId the {@link NexusServer} Id
	 * @return the {@link NexusServer}, may be <code>null</code>
	 */
	NexusServer getServer(int serverId);

	/**
	 * Getter for a {@link NexusServer} by its name
	 * 
	 * @param name the {@link NexusServer} name
	 * @return the {@link NexusServer}, may be <code>null</code>
	 */
	NexusServer getServer(String name);

	/**
	 * Add a {@link NexusServer}
	 *
	 * @param name the name of the {@link NexusServer}
	 * @param description the description of the {@link NexusServer}
	 * @param host the base host of the {@link NexusServer} 
	 * @param username the username to use for authentication
	 * @param password the password to use for authentication
	 * @return the new {@link NexusServer}
	 */
	NexusServer addServer(String name, String description, String host, String username, String password);

	/**
	 * Update a {@link NexusServer}
	 * 
	 * @param serverId the Id of the server to update
	 * @param name the name of the {@link NexusServer}
	 * @param description the description of the {@link NexusServer}
	 * @param host the base host of the {@link NexusServer}
	 * @param username the username to use for authentication
	 * @param password the password to use for authentication
	 * @return the new {@link NexusServer}
	 */
	NexusServer updateServer(int serverId, String name, String description, String host, String username,
					String password);

	/**
	 * Remove a {@link NexusServer} by its server Id
	 * 
	 * @param serverId the Id of the {@link NexusServer} to remove
	 */
	void removeServer(int serverId);

}
