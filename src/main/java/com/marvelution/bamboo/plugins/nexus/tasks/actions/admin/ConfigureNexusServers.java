/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.actions.admin;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.sonatype.nexus.client.rest.BaseUrl;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusServerTaskConfigurationService;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServer;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServerManager;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@SuppressWarnings("unchecked")
public class ConfigureNexusServers extends BambooActionSupport implements GlobalAdminSecurityAware, InvocationHandler {

	/**
	 * ADD Mode type string value
	 */
	public static final String ADD = "add";

	/**
	 * EDIT Mode type string value
	 */
	public static final String EDIT = "edit";

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(ConfigureNexusServers.class);

	private NexusServerManager serverManager;
	private NexusServerTaskConfigurationService serverTaskConfigrationService;
	private String action;
	private String mode;
	private int serverId;
	private String serverName;
	private String serverDescription;
	private String serverHost;
	private String serverUsername;
	private String serverPassword;

	/**
	 * Setter for {@link NexusServerManager}
	 *
	 * @param serverManager the {@link NexusServerManager} to set
	 */
	public void setServerManager(NexusServerManager serverManager) {
		this.serverManager = serverManager;
	}

	/**
	 * Setter for {@link NexusServerTaskConfigurationService}
	 *
	 * @param serverTaskConfigrationService the {@link NexusServerTaskConfigurationService} to set
	 */
	public void setServerTaskConfigrationService(NexusServerTaskConfigurationService serverTaskConfigrationService) {
		this.serverTaskConfigrationService = serverTaskConfigrationService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		if (StringUtils.isNotBlank(getAction())) {
			addActionMessage(getText("nexus.global.messages.server.action",
				new String[] {getText("nexus.global.messages.server.action." + getAction())}));
		}
		return SUCCESS;
	}

	/**
	 * Add a {@link NexusServer}
	 * 
	 * @return the view to show to add the {@link NexusServer}
	 * @throws Exception in case of errors
	 */
	public String doAdd() throws Exception {
		setMode(ADD);
		logger.debug("Adding a new Nexus Server, return the Add form view");
		return INPUT;
	}

	/**
	 * Create a {@link NexusServer}
	 * 
	 * @return the view to show when the {@link NexusServer} is created
	 * @throws Exception in case of errors when creating the server
	 */
	public String doCreate() throws Exception {
		setMode(ADD);
		if (!isValidServer()) {
			return ERROR;
		}
		NexusServer server = serverManager.addServer(getServerName(), getServerDescription(), getServerHost(),
			getServerUsername(), getServerPassword());
		logger.debug("Added server with name " + server.getName() + " under ID " + server.getID());
		return SUCCESS;
	}

	/**
	 * Edit a {@link NexusServer}
	 * 
	 * @return the view to show to edit the {@link NexusServer}
	 * @throws Exception in case of errors
	 */
	public String doEdit() throws Exception {
		setMode(EDIT);
		logger.debug("Editing a Nexus Server, return the Edit form view");
		if (!serverManager.hasServer(getServerId())) {
			addActionError(getText("nexus.global.errors.no.server.id", new String[] {"update"}));
			return ERROR;
		} else {
			NexusServer server = serverManager.getServer(getServerId());
			setServerName(server.getName());
			setServerDescription(server.getDescription());
			setServerHost(server.getHost());
			setServerUsername(server.getUsername());
			setServerPassword(server.getPassword());
		}
		return INPUT;
	}

	/**
	 * Update a {@link NexusServer}
	 * 
	 * @return the view to show when the {@link NexusServer} is updated
	 * @throws Exception in case of errors when updating the server
	 */
	public String doUpdate() throws Exception {
		setMode(EDIT);
		if (!isValidServer()) {
			return ERROR;
		} else if (!serverTaskConfigrationService.updateNexusServerTaskConfiguration(getNexusServer())) {
			addActionError(getText("nexus.global.errors.task.validation.errors"));
			return ERROR;
		}
		NexusServer server = serverManager.updateServer(getServerId(), getServerName(), getServerDescription(),
			getServerHost(), getServerUsername(), getServerPassword());
		logger.debug("Updated server with ID " + server.getID());
		return SUCCESS;
	}

	/**
	 * Delete a {@link NexusServer}
	 * 
	 * @return the view to show to delete the {@link NexusServer}
	 * @throws Exception in case of errors
	 */
	public String doDelete() throws Exception {
		setMode(EDIT);
		logger.debug("Deleting a Nexus Server, return the Delete form view");
		if (!serverManager.hasServer(getServerId())) {
			addActionError(getText("nexus.global.errors.no.server.id", new String[] {"delete"}));
			return ERROR;
		} else {
			NexusServer server = serverManager.getServer(getServerId());
			setServerName(server.getName());
			setServerDescription(server.getDescription());
			setServerHost(server.getHost());
		}
		return INPUT;
	}

	/**
	 * Remove a {@link NexusServer}
	 * 
	 * @return the view to show when the {@link NexusServer} is removed
	 * @throws Exception in case of errors when removing the server
	 */
	public String doRemove() throws Exception {
		if (serverManager.hasServer(getServerId())) {
			NexusServer server = serverManager.getServer(serverId);
			serverTaskConfigrationService.disableNexusServerTaskJobs(server);
			serverManager.removeServer(getServerId());
			return SUCCESS;
		} else {
			addActionError(getText("nexus.global.errors.no.server.id", new String[] {"delete"}));
			return ERROR;
		}
	}

	/**
	 * Internal method to valid the Nexus Server settings
	 * 
	 * @return <code>true</code> if valid, <code>false</code> otherwise
	 * @throws Exception
	 */
	private boolean isValidServer() throws Exception {
		if (StringUtils.isNotBlank(getServerName())) {
			NexusServer server = serverManager.getServer(getServerName());
			if (server != null && server.getID() != getServerId()) {
				addFieldError("serverName", getText("nexus.server.name.no.duplicates"));
			}
		} else if (StringUtils.isBlank(getServerName())) {
			addFieldError("serverName", getText("nexus.server.name.required"));
		}
		if (StringUtils.isBlank(getServerHost())) {
			addFieldError("serverHost", getText("nexus.host.url.required"));
		} else if (!getServerHost().startsWith("http://") && !getServerHost().startsWith("https://")) {
			addFieldError("serverHost", getText("nexus.host.url.invalid"));
		}
		try {
			BaseUrl.baseUrlFrom(getServerHost());
		} catch (MalformedURLException e) {
			addFieldError("serverHost", getText("nexus.host.url.malformed"));
		}
		if (StringUtils.isBlank(getServerUsername())) {
			addFieldError("serverUsername", getText("nexus.host.username.required"));
		}
		if (StringUtils.isBlank(getServerPassword())) {
			addFieldError("serverPassword", getText("nexus.host.password.required"));
		}
		return !(hasFieldErrors() || hasActionErrors());
	}

	/**
	 * Get all the {@link NexusServer} instances
	 * 
	 * @return {@link Collection} of {@link NexusServer} instances
	 */
	public Collection<NexusServer> getNexusServers() {
		return serverManager.getServers();
	}

	/**
	 * Getter for action
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Setter for action
	 *
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Getter for mode
	 *
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Setter for mode
	 *
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * Getter for serverId
	 *
	 * @return the serverId
	 */
	public int getServerId() {
		return serverId;
	}

	/**
	 * Setter for serverId
	 *
	 * @param serverId the serverId to set
	 */
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	/**
	 * Getter for serverName
	 *
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * Setter for serverName
	 *
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * Getter for serverDescription
	 *
	 * @return the serverDescription
	 */
	public String getServerDescription() {
		return serverDescription;
	}

	/**
	 * Setter for serverDescription
	 *
	 * @param serverDescription the serverDescription to set
	 */
	public void setServerDescription(String serverDescription) {
		this.serverDescription = serverDescription;
	}

	/**
	 * Getter for serverHost
	 *
	 * @return the serverHost
	 */
	public String getServerHost() {
		return serverHost;
	}

	/**
	 * Setter for serverHost
	 *
	 * @param serverHost the serverHost to set
	 */
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	/**
	 * Getter for serverUsername
	 *
	 * @return the serverUsername
	 */
	public String getServerUsername() {
		return serverUsername;
	}

	/**
	 * Setter for serverUsername
	 *
	 * @param serverUsername the serverUsername to set
	 */
	public void setServerUsername(String serverUsername) {
		this.serverUsername = serverUsername;
	}

	/**
	 * Getter for serverPassword
	 *
	 * @return the serverPassword
	 */
	public String getServerPassword() {
		return serverPassword;
	}

	/**
	 * Setter for serverPassword
	 *
	 * @param serverPassword the serverPassword to set
	 */
	public void setServerPassword(String serverPassword) {
		this.serverPassword = serverPassword;
	}

	/**
	 * Internal method to get a {@link Proxy} for the {@link NexusServer}
	 * This {@link Proxy} has the {@link ConfigureNexusServers} object as {@link InvocationHandler}
	 * 
	 * @return the {@link Proxy}
	 */
	private NexusServer getNexusServer() {
		return (NexusServer) Proxy.newProxyInstance(NexusServer.class.getClassLoader(),
			new Class[] {NexusServer.class}, this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		logger.debug("Got a NexusServer Proxy Invocation for " + method.getName());
		if (method.getName().equals("getID")) {
			return getServerId();
		} else if (method.getName().equals("getName")) {
			return getServerName();
		} else if (method.getName().equals("getDescription")) {
			return getServerDescription();
		} else if (method.getName().equals("getHost")) {
			return getServerHost();
		} else if (method.getName().equals("getUsername")) {
			return getServerUsername();
		} else if (method.getName().equals("getPassword")) {
			return getServerPassword();
		}
		return null;
	}

}
