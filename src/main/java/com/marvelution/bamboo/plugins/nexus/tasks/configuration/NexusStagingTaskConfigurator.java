/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.configuration;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.sonatype.nexus.client.rest.BaseUrl;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServer;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServerManager;

/**
 * TaskConfigurator implementation for the Nexus Staging tasks
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class NexusStagingTaskConfigurator extends AbstractTaskConfigurator implements NexusConfigurationConstants {

	private static final List<String> FIELDS_TO_COPY = ImmutableList.of(CFG_BUILDER_LABEL, CFG_NEXUS_ID,
		CFG_NEXUS_URL, CFG_NEXUS_USERNAME, CFG_STAGING_PROFILE_ID, CFG_GAV_OPTION, CFG_GROUP_ID, CFG_ARTIFACT_ID,
		CFG_VERSION, CFG_DESCRIPTION, CFG_POM_FILE);

	private UIConfigSupport uiConfigBean;
	private NexusServerManager serverManager;

	@NotNull
	@Override
	public Map<String, String> generateTaskConfigMap(ActionParametersMap params, TaskDefinition previousTaskDefinition) {
		Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
		taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
		if (params.containsKey(CFG_NEXUS_ID) && serverManager.hasServer(params.getInt(CFG_NEXUS_ID, 0))) {
			// Copy the Nexus server to the ActionParametersMap in case it is set
			NexusServer server = serverManager.getServer(params.getInt(CFG_NEXUS_ID, 0));
			config.put(CFG_NEXUS_URL, server.getHost());
			config.put(CFG_NEXUS_USERNAME, server.getUsername());
			config.put(CFG_NEXUS_PASSWORD, ENCRYPTOR.encrypt(server.getPassword()));
		} else {
			// Encrypt the password before adding it to the configuration
			if (params.getBoolean(CTX_CHANGE_PASSWORD)) {
				config.put(CFG_NEXUS_PASSWORD, ENCRYPTOR.encrypt(params.getString(CTX_TEMPORARY_PASSWORD)));
			} else if (previousTaskDefinition != null
				&& StringUtils.isNotBlank(previousTaskDefinition.getConfiguration().get(CFG_NEXUS_PASSWORD))) {
				config.put(CFG_NEXUS_PASSWORD, previousTaskDefinition.getConfiguration().get(CFG_NEXUS_PASSWORD));
			}
		}
		return config;
	}

	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		super.populateContextForCreate(context);
		populateContextForAllOperations(context);
		context.put(CFG_NEXUS_ID, "0");
		context.put(CFG_GAV_OPTION, CFG_AUTO_DISCOVER_GAV);
		context.put(CFG_STAGING_PROFILE_ID, CTX_IMPLICIT_PROFILE_ID);
	}

	@Override
	public void populateContextForEdit(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		populateContextForAllOperations(context);
		taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition,
			Iterables.concat(FIELDS_TO_COPY, ImmutableList.of(CFG_NEXUS_PASSWORD)));
	}

	@Override
	public void populateContextForView(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForView(context, taskDefinition);
		populateContextForAllOperations(context);
		taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
		context.put(CTX_USES_AUTO_GAV,
			CFG_AUTO_DISCOVER_GAV.equals(taskDefinition.getConfiguration().get(CFG_GAV_OPTION)));
		try {
			int nexusId = Integer.parseInt(taskDefinition.getConfiguration().get(CFG_NEXUS_ID));
			if (serverManager.hasServer(nexusId)) {
				context.put(CTX_NEXUS_SERVER, serverManager.getServer(nexusId));
			} else if (nexusId > 0) {
				context.put(CTX_DELETED_SERVER, true);
			}
		} catch (Exception e) {
			context.put(CFG_NEXUS_ID, "0");
		}
		// Override the password field in View mode for security reasons
		context.put(CFG_NEXUS_PASSWORD, CTX_FAKE_PASSWORD);
	}

	@Override
	public void validate(ActionParametersMap params, ErrorCollection errorCollection) {
		super.validate(params, errorCollection);
		if (params.containsKey(CFG_NEXUS_ID) && params.getInt(CFG_NEXUS_ID, 0) != 0) {
			if (!serverManager.hasServer(params.getInt(CFG_NEXUS_ID, 0))) {
				errorCollection.addError(CFG_NEXUS_ID, getI18nBean().getText("nexus.server.unknown"));
			}
		} else {
			if (StringUtils.isBlank(params.getString(CFG_NEXUS_URL))) {
				errorCollection.addError(CFG_NEXUS_URL, getI18nBean().getText("nexus.host.url.required"));
			} else if (!params.getString(CFG_NEXUS_URL).startsWith("http://")
					&& !params.getString(CFG_NEXUS_URL).startsWith("http://")) {
				errorCollection.addError(CFG_NEXUS_URL, getI18nBean().getText("nexus.host.url.invalid"));
			}
			try {
				BaseUrl.baseUrlFrom(params.getString(CFG_NEXUS_URL));
			} catch (MalformedURLException e) {
				errorCollection.addError(CFG_NEXUS_URL, getI18nBean().getText("nexus.host.url.malformed"));
			}
		}
		if (StringUtils.isBlank(params.getString(CFG_DESCRIPTION))) {
			errorCollection.addError(CFG_DESCRIPTION, getI18nBean().getText("nexus.action.description.required"));
		}
		if (CTX_IMPLICIT_PROFILE_ID.equals(params.getString(CFG_STAGING_PROFILE_ID, CTX_IMPLICIT_PROFILE_ID))
				&& CFG_PROVIDE_GAV.equals(params.getString(CFG_GAV_OPTION))) {
			if (StringUtils.isBlank(params.getString(CFG_GROUP_ID))) {
				errorCollection.addError(CFG_GROUP_ID, getI18nBean().getText("nexus.gav.groupId.required"));
			}
			if (StringUtils.isBlank(params.getString(CFG_ARTIFACT_ID))) {
				errorCollection.addError(CFG_ARTIFACT_ID, getI18nBean().getText("nexus.gav.artifactId.required"));
			}
		}
	}

	/**
	 * Setter for the {@link UIConfigSupport}
	 * 
	 * @param uiConfigBean the {@link UIConfigSupport}
	 */
	public void setUiConfigBean(UIConfigSupport uiConfigBean) {
		this.uiConfigBean = Preconditions.checkNotNull(uiConfigBean, "uiConfigBean may not be null");
	}

	/**
	 * Setter for the {@link NexusServerManager}
	 *
	 * @param serverManager the {@link NexusServerManager}
	 */
	public void setServerManager(NexusServerManager serverManager) {
		this.serverManager = Preconditions.checkNotNull(serverManager, "serverManager may not be null");
	}

	/**
	 * Setter for the context that is applicable for both view and edit
	 * 
	 * @param context the {@link Map} context to set
	 */
	private void populateContextForAllOperations(@NotNull Map<String, Object> context) {
		context.put(CTX_UI_CONFIG_BEAN, uiConfigBean);
		context.put(CTX_GAV_OPTIONS, ImmutableMap.of(
			CFG_AUTO_DISCOVER_GAV, getI18nBean().getText("nexus.gav.auto.discovery"),
			CFG_PROVIDE_GAV, getI18nBean().getText("nexus.gav.provided")
		));
		Map<String, String> servers = Maps.newHashMap();
		servers.put("0", getI18nBean().getText("nexus.server.specify"));
		for (NexusServer server : serverManager.getServers()) {
			servers.put(String.valueOf(server.getID()), server.getName());
		}
		context.put(CTX_NEXUS_SERVERS, servers);
	}

}
