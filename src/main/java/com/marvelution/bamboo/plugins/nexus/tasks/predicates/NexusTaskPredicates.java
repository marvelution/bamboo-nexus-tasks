/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.nexus.tasks.predicates;

import static com.marvelution.bamboo.plugins.nexus.tasks.configuration.NexusConfigurationConstants.CFG_NEXUS_ID;

import com.marvelution.bamboo.plugins.nexus.utils.PluginHelper;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.build.Buildable;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskIdentifier;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.marvelution.bamboo.plugins.nexus.tasks.servers.NexusServer;

/**
 * Nexus related {@link Predicate} helper
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class NexusTaskPredicates {

	/**
	 * Get the Has Nexus Tasks {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	public static Predicate<Buildable> hasNexusTasks() {
		return new Predicate<Buildable>() {
			@Override
			public boolean apply(Buildable buildable) {
				return Iterables.any(buildable.getBuildDefinition().getTaskDefinitions(), isNexusTask());
			}
		};
	}

	/**
	 * Get the Is Nexus Task {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	public static IsNexusTaskPredicate<TaskDefinition> isNexusTask() {
		return new IsNexusTaskPredicate<TaskDefinition>();
	}

	/**
	 * Get the Is Nexus Server dependency task {@link Predicate}
	 * 
	 * @param server
	 * @return {@link Predicates#and(Predicate, Predicate)} implementation combining the {@link #isNexusTask()} and
	 *         the {@link IsNexusConfigurationSettingDependentPredicate} predicates
	 */
	public static Predicate<TaskDefinition> isNexusServerDependingTask(final NexusServer server) {
		return Predicates.and(isNexusTask(),
			new IsNexusConfigurationSettingDependentPredicate(CFG_NEXUS_ID, String.valueOf(server.getID())));
	}

	/**
	 * Get the Is Nexus Staging Close task {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	public static IsNexusTaskTypePredicate<TaskDefinition> isNexusStagingCloseTask() {
		return new IsNexusTaskTypePredicate<TaskDefinition>("close");
	}

	/**
	 * Get the is Nexus Staging Drop task {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	public static IsNexusTaskTypePredicate<TaskDefinition> isNexusStagingDropTask() {
		return new IsNexusTaskTypePredicate<TaskDefinition>("drop");
	}

	/**
	 * Get the is Nexus Staging Promote task {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	public static IsNexusTaskTypePredicate<TaskDefinition> isNexusStagingReleaseTask() {
		return new IsNexusTaskTypePredicate<TaskDefinition>("release");
	}

	/**
	 * Get the is Nexus Staging task {@link Predicate}
	 * 
	 * @return the {@link Predicate}
	 */
	@SuppressWarnings("unchecked")
	public static Predicate<TaskDefinition> isNexusStagingTask() {
		return Predicates.or(isNexusStagingCloseTask(), isNexusStagingDropTask(), isNexusStagingReleaseTask());
	}

	/**
	 * Specific Nexus Task {@link Predicate} implementation
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @param <TASKDEF>
	 *
	 * @since 1.0.0
	 */
	private static class IsNexusTaskTypePredicate<TASKDEF extends TaskIdentifier> implements Predicate<TASKDEF> {

		private final String taskTypeKey;

		/**
		 * Default Constructor
		 * 
		 * @param taskTypeKey the Nexus TaskType key
		 */
		protected IsNexusTaskTypePredicate(String taskTypeKey) {
			this.taskTypeKey = taskTypeKey;
		}

		@Override
		public boolean apply(@Nullable TaskIdentifier taskIdentifier) {
			return ((TaskIdentifier) Preconditions.checkNotNull(taskIdentifier)).getPluginKey().equals(
				PluginHelper.getPluginKey() + ":task.nexus." + taskTypeKey);
		}

		@Override
		public String toString() {
			return IsNexusTaskTypePredicate.class.getName() + " " + PluginHelper.getPluginKey() + ":task.nexus." + taskTypeKey;
		}

	}

	/**
	 * Generic "Is Nexus" {@link Predicate} implementation
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @param <TASKDEF>
	 *
	 * @since 1.0.0
	 */
	private static class IsNexusTaskPredicate<TASKDEF extends TaskIdentifier> implements Predicate<TASKDEF> {

		@Override
		public boolean apply(@Nullable TASKDEF taskIdentifier) {
			return ((TaskIdentifier) Preconditions.checkNotNull(taskIdentifier)).getPluginKey().startsWith(
				PluginHelper.getPluginKey() + ":task.nexus.");
		}

		@Override
		public String toString() {
			return IsNexusTaskPredicate.class.getName() + " " + PluginHelper.getPluginKey() + ":task.nexus.";
		}

	}

	/**
	 * {@link Predicate} implementation to check on specific task configuration field value
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @since 1.0.0
	 */
	private static class IsNexusConfigurationSettingDependentPredicate implements Predicate<TaskDefinition> {

		private final String name;
		private final String value;

		/**
		 * Constructor
		 *
		 * @param name the configuration field name
		 * @param value the configuration field value
		 */
		protected IsNexusConfigurationSettingDependentPredicate(String name, String value) {
			this.name = Preconditions.checkNotNull(name);
			this.value = Preconditions.checkNotNull(value);
		}

		@Override
		public boolean apply(@Nullable TaskDefinition taskDefinition) {
			Preconditions.checkNotNull(taskDefinition);
			return taskDefinition.getConfiguration().containsKey(name)
				&& StringUtils.equals(taskDefinition.getConfiguration().get(name), value);
		}

		@Override
		public String toString() {
			return IsNexusConfigurationSettingDependentPredicate.class.getName() + "[" +name + "|" + value + "]";
		}

	}

}
