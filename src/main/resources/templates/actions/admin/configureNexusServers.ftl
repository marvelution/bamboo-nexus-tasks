[#--
 ~ Licensed to Marvelution under one or more contributor license
 ~ agreements.  See the NOTICE file distributed with this work
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 --]

[#-- @ftlvariable name="action" type="com.marvelution.bamboo.plugins.nexus.tasks.actions.admin.ConfigureNexusServers" --]
[#-- @ftlvariable name="" type="com.marvelution.bamboo.plugins.nexus.tasks.actions.admin.ConfigureNexusServers" --]

[#if mode == 'edit' ]
	[#assign targetAction = "/admin/nexus/updateNexusServer.action"]
[#else]
	[#assign targetAction = "/admin/nexus/createNexusServer.action"]
[/#if]

<html>
<head>
	[@ui.header pageKey="nexus.global.${mode}.server.title" title=true /]
	<meta name="decorator" content="adminpage">
</head>
<body>
	[#include "serverRunningWarning.ftl"]
	[@ui.header pageKey="nexus.global.${mode}.server.heading" /]
	<p>[@ww.text name='nexus.global.${mode}.server.description' /]</p>
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey='nexus.global.${mode}.server.button'
			titleKey='nexus.global.${mode}.server.form.title'
			cancelUri='/admin/nexus/viewNexusServers.action'
			showActionErrors='true'
			descriptionKey='nexus.global.${mode}.server.form.description']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='serverId' /]
		[/#if]
		[@ww.textfield name='serverName' labelKey='nexus.server.name' descriptionKey='nexus.server.name.description' required='true' /]
		[@ww.textfield name='serverDescription' labelKey='nexus.server.description' descriptionKey='nexus.server.description.description' /]
		[@ww.textfield name='serverHost' labelKey='nexus.host.url' descriptionKey='nexus.host.url.description' required='true' /]
		[@ww.textfield name='serverUsername' labelKey='nexus.host.username' descriptionKey='nexus.host.username.description' required='true' /]
		[@ww.password name='serverPassword' labelKey='nexus.host.password' descriptionKey='nexus.host.password.description' required='true' showPassword='true' /]
	[/@ww.form]
</body>
</html>
