[#--
 ~ Licensed to Marvelution under one or more contributor license
 ~ agreements.  See the NOTICE file distributed with this work
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 --]

[#if nexusId != "0" && nexusServer?has_content ]
	[@ww.label labelKey='nexus.server.global' escape="false"]
		[@ww.param name='value' ]${nexusServer.name}[/@ww.param]
	[/@ww.label]
	[@ww.label labelKey='nexus.host.url' escape="false"]
		[@ww.param name='value' ]<i>${nexusServer.host}</i>[/@ww.param]
	[/@ww.label]
	[@ww.label labelKey='nexus.host.username' escape="false"]
		[@ww.param name='value' ]<i>${nexusServer.username}</i>[/@ww.param]
	[/@ww.label]
	[@ww.label labelKey='nexus.host.password' name='hostPassword' /]
[#else]
	[#if deletedServer?has_content ]
		[@ww.label labelKey='nexus.server.global' escape="false"]
			[@ww.param name='value' ]<b>[@ww.text name="nexus.server.global.deleted" /]</b>[/@ww.param]
		[/@ww.label]
	[/#if]
	[@ww.label labelKey='nexus.host.url' name='hostUrl' /]
	[@ww.label labelKey='nexus.host.username' name='hostUsername' /]
	[@ww.label labelKey='nexus.host.password' name='hostPassword' /]
[/#if]

[#if stagingProfileId?has_content ]
	[@ww.label labelKey='nexus.staging.profileId' name='stagingProfileId' /]
[#else]
	[#if usesAutoGav ]
		[@ww.label labelKey='nexus.gav.discovery' escape="false"]
			[@ww.param name='value' ]
				[@ww.text name="nexus.gav.auto.discovery" /]
			[/@ww.param]
		[/@ww.label]
		[@ww.label labelKey='nexus.gav.mavenPom' name='mavenPom' hideOnNull='true' /]
	[#else]
		[@ww.label labelKey='nexus.gav.discovery' escape="false"]
			[@ww.param name='value' ]
				[@ww.text name="nexus.gav.provided" /]
			[/@ww.param]
		[/@ww.label]
		[@ww.label labelKey='nexus.gav.groupId' name='groupId' /]
		[@ww.label labelKey='nexus.gav.artifactId' name='artifactId' /]
		[@ww.label labelKey='nexus.gav.version' name='version' hideOnNull='true' /]
	[/#if]
[/#if]
[@ww.label labelKey='nexus.action.description' name='description' /]
