[#--
 ~ Licensed to Marvelution under one or more contributor license
 ~ agreements.  See the NOTICE file distributed with this work
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 --]

[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[@ui.bambooSection titleKey='nexus.host.configuration']
	[@ww.select labelKey='nexus.server.options' name='nexusId'
				listKey='key' listValue='value' toggle='true'
				list=nexusServers /]
	[@ui.bambooSection dependsOn='nexusId' showOn='0']
		[@ww.textfield labelKey='nexus.host.url' name='hostUrl' required='true' /]
		[@ww.textfield labelKey='nexus.host.username' name='hostUsername' /]
		[#if hostPassword?has_content]
			[@ww.checkbox labelKey='nexus.host.password.change' toggle=true name='host.password.change' /]
			[@ui.bambooSection dependsOn='host.password.change' showOn=true]
				[@ww.password labelKey='nexus.host.password' name='host.temporary.password' /]
			[/@ui.bambooSection]
		[#else]
			[@ww.hidden name='host.password.change' value=true /]
			[@ww.password labelKey='nexus.host.password' name='host.temporary.password' /]
		[/#if]
	[/@ui.bambooSection]
[/@ui.bambooSection]
[@ui.bambooSection titleKey='nexus.staging.filters']
	[@ww.select labelKey='nexus.staging.profileId' name='stagingProfileId' fieldClass='nexus-profile' cssClass='nexus-profile-selector' toggle='true']
	    [@ww.param name='disabled' value=!(stagingProfileId?has_content) /]
	    [@ww.param name='extraUtility']
	    	[@ui.displayButton id='nexus-staging-load-profiles' valueKey='nexus.load.profiles'/]
			[@ww.hidden name="profileTypes" value="staging" /]
	    [/@ww.param]
	    [#if stagingProfileId?has_content]
	        [@ww.param name='headerKey2' value=stagingProfileId /]
	        [@ww.param name='headerValue2' value=stagingProfileId /]
	    [/#if]
	[/@ww.select]
	[@ui.bambooSection dependsOn='stagingProfileId' showOn='implicit']
		[@ui.bambooSection titleKey='nexus.gav.discovery.method']
			[@ww.radio labelKey='nexus.gav.discovery.option' name='gavOption'
						listKey='key' listValue='value' toggle='true'
						list=gavOptions ]
			[/@ww.radio]
			[@ui.bambooSection dependsOn='gavOption' showOn='autoDiscoverGav']
				[@ww.textfield labelKey='nexus.gav.mavenPom' name='mavenPom' /]
			[/@ui.bambooSection]
			[@ui.bambooSection dependsOn='gavOption' showOn='provideGav']
				[@ww.textfield labelKey='nexus.gav.groupId' name='groupId' required='true' /]
				[@ww.textfield labelKey='nexus.gav.artifactId' name='artifactId' required='true' /]
				[@ww.textfield labelKey='nexus.gav.version' name='version' /]
			[/@ui.bambooSection]
		[/@ui.bambooSection]
	[/@ui.bambooSection]
[/@ui.bambooSection]
[@ui.bambooSection titleKey='nexus.action.details']
	[@ww.textarea labelKey='nexus.action.description' name='description' rows='4' required='true' cssClass="long-field" /]
[/@ui.bambooSection]

<script type="text/javascript">
(function () {
    var stagingForm = new BAMBOO.NEXUS.StagingForm({
    	password: "${(hostPassword)!}",
        selectors: {
        	nexus: 'select[name="nexusId"]',
        	nexusUrl: 'input[name="hostUrl"]',
            username: 'input[name="hostUsername"]',
            password: 'input[name="host.temporary.password"]',
            profiles: 'select[class~="nexus-profile-selector"]'
        }
    });
    stagingForm.init();
}());
</script>
