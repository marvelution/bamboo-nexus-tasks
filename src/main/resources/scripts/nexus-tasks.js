/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

(function($, BAMBOO) {
	BAMBOO.NEXUS = {};

    /**
     * Nexus Staging form
     * 
     * @param {Object} opts - options with which to set up the form
     * @constructor
     */
	function StagingForm(opts) {
		var defaults = {
				password: null,
				selectors: {
					nexus: null,
		        	nexusUrl: null,
		            username: null,
		            password: null,
		            profiles: null
				}
			};
		// Options
		this.options = $.extend(true, defaults, opts);
		// Fields
		this.$nexus = $(this.options.selectors.nexus);
		this.$nexusUrl = $(this.options.selectors.nexusUrl);
		this.$username = $(this.options.selectors.username);
		this.$password = $(this.options.selectors.password);
		this.$profiles = $(this.options.selectors.profiles);
		this.jqXHRs = {};
	}

	StagingForm.prototype = {
		init : function() {
			// Attach handlers
			this.$profiles.siblings('input[type="button"]').click(_.bind(this.loadProfilesClick, this));
			// Load profiles if needed
			if (this.$nexus.val() || this.$nexusUrl.val()) {
				if (this.$profiles.length > 0) {
					_.each(this.$profiles, function(element, index, list) {
						this.loadProfiles($(element));
						$(element).change(function(event) {
							BAMBOO.DynamicFieldParameters.syncFieldShowHide($(event.target).parent(), true);
						});
					}, this);
				}
			}
		},
		loadProfilesClick: function(event) {
			var $field = $(event.target).siblings(this.options.selectors.profiles);
			this.clearFieldErrors($field);
			$field.prop('disabled', true);
			this.loadProfiles($field);
		},
		loadProfiles: function($selector) {
			var $button = $selector.siblings('input[type="button"]'),
				$loadingOptgroup = $('<optgroup />').attr('label', AJS.I18n.getText('nexus.loading.profiles')).appendTo($selector),
				type = $selector.siblings('input[type="hidden"][name="profileTypes"]').val(),
				required = this.getFieldArea($selector).hasClass('required'),
				currentlySelected = $selector.val(),
				generateOption = this.generateOption,
				update = _.bind(function(json, textStatus, jqXHR) {
					var data = json['data'];
					if (data) {
						var $list = $();
						if (!required) {
							$list = $list.add(generateOption({
								id: 'implicit',
								name: AJS.I18n.getText('nexus.implicit.profile')
							}));
						}
						$.each(data, function() {
							$list = $list.add(generateOption(this));
						});
						$selector.empty().append($list).prop('disabled', false).val(currentlySelected).change();
					} else {
						showError(jqXHR);
					}
				}, this),
				restore = _.bind(function () {
					$loadingOptgroup.remove();
					$button.prop('disabled', false);
				}, this),
				showError = _.bind(function (jqXHR) {
					var response, errors = [];
                    if (jqXHR.statusText != 'abort') {
                        try {
                            response = $.parseJSON(jqXHR.responseText);
                            errors = response['errors'];
                        } catch (e) {};
                        if (!errors.length) {
                            errors.push(AJS.I18n.getText('nexus.rest.ajaxError') + '[' + jqXHR.status + ' ' + jqXHR.statusText + ']');
                        }
                        this.addFieldErrors($selector, errors);
                    }
				}, this);
			$button.prop('disabled', true);
			return this.getProfiles(type, $selector).done(update).fail(showError).always(restore);
		},
		generateOption: function(profile) {
			return $('<option/>', {
				text : profile['name'],
				val : profile['id'],
				data : profile
			});
		},
		getProfiles: function(type, $field) {
			var url = AJS.contextPath() + '/rest/nexus/latest/staging/profiles/' + type,
				$container = $field.closest('.field-group'),
				loadingClass = 'loading',
				data = {};
			if (this.$nexus.val() != 0) {
				data.nexusId = this.$nexus.val();
			} else {
				data.baseUrl = this.$nexusUrl.val();
				data.username = this.$username.val();
				if (this.$password.val()) {
					data.password = this.$password.val();
					data.encrypted = false;
				} else if (this.options.password) {
					data.password = this.options.password;
					data.encrypted = true;
				}
			}
			if (this.jqXHRs[type]) {
				this.jqXHRs[type].abort();
			}
			$container.addClass(loadingClass);
			return this.jqXHRs[type] = $.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'json'
			}).always(function() {
				$container.removeClass(loadingClass);
			});
		},
		addFieldErrors : function($field, errors) {
			var $fieldArea = this.getFieldArea($field),
				$description = $fieldArea.find('.description'),
				$errors = BAMBOO.buildFieldError(errors);
			$errors.hide();
			if ($description.length) {
				$description.before($errors);
			} else if ($fieldArea.length) {
				$fieldArea.append($errors);
			} else {
				$field.after($errors);
			}
			$errors.slideDown();
		},
		clearFieldErrors : function($field) {
			this.getFieldArea($field).find('.error').slideUp(function() {
				$(this).remove();
			});
		},
		getFieldArea: function($field) {
			return $field.closest('.nexus-profile');
		}
	}

	BAMBOO.NEXUS.StagingForm = StagingForm;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));
