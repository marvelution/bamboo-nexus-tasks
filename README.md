This project features Bamboo Tasks for executing Sonatype Nexus Staging actions
More details on <https://marvelution.atlassian.net/wiki/display/BAMNEX>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/BAMNEX>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
